import os
import grpc
import argparse

from maum.brain.insight.insight_pb2 import FaceImage
from maum.brain.insight.insight_pb2_grpc import InsightFaceStub


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='face model client example code')
    parser.add_argument('-i', '--input_image', type=str, default='sample_image/sample_001.png',
                        help='input image path')
    parser.add_argument('--model', default='/root/.insightface/models/model-r100-ii/model,0',
                        help='path to load model.')
    parser.add_argument('--ip', default='0.0.0.0', type=str, help='server ip')
    parser.add_argument('-p', '--port', default='56101', type=str, help='server port')
    parser.add_argument('--mode', choices=["GET_VECTOR", "SET_FACE", "RECOG_FACE"],
                        default='RECOG_FACE', help='client mode for request')

    args = parser.parse_args()

    face_image = FaceImage()
    with open("sample_image.png", "rb") as f:
        face_image.faceImg = f.read()
    if args.mode == "GET_VECTOR":
        with grpc.insecure_channel("{:s}:{:s}".format(args.ip, args.port)) as channel:
            stub = InsightFaceStub(channel)
            print(stub.GetFaceVector(face_image).faceVector)
    elif args.mode == "SET_FACE":
        face_image.name = "test"
        with grpc.insecure_channel("{:s}:{:s}".format(args.ip, args.port)) as channel:
            stub = InsightFaceStub(channel)
            print(stub.SetFace(face_image).success)
    elif args.mode == "RECOG_FACE":
        with grpc.insecure_channel("{:s}:{:s}".format(args.ip, args.port)) as channel:
            stub = InsightFaceStub(channel)
            ret = stub.RecogFace(face_image)
            print(ret.name, ret.success)
