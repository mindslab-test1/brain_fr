# brain_fr

## Main compatibility
MXNET 1.7, CUDA 10.0

## Docker run command

```shell script
docker run -itd --ipc=host --gpus="all" -v /path/to/faces_emore:/DATA1/faces_emore \
-v /path/to/models:/root/.insightface/models -v /path/to/sample_image:/app/sample_image -p 56101:56101 --name brain_fr brain_fr:v1.0.0
```
For example, it can be done as
```shell script
docker run -itd --ipc=host --gpus="all" -v /DATA1/faces_emore:/DATA1/faces_emore \
-v /DATA1/brain_fr/models:/root/.insightface/models -v /DATA1/brain_fr/sample_image:/app/sample_image -p 56101:56101 --name brain_fr brain_fr:v1.0.0
```

Then, to execute docker container, you may type
```shell script
docker exec -it brain_fr bash
```
## Functionality

### Training the model

Training with single V100 GPU.
It takes about 4 weeks to be trained well.
The dataset consists of several face datasets.
Please refer to [the original repo's readme](https://github.com/deepinsight/insightface/wiki/Dataset-Zoo).
There are pre-trained checkpoints with those dataset.

```shell script
cd /app/recognition/ArcFace
# simply run
bash train.sh
# or you may type it manually as 
CUDA_VISIBLE_DEVICES=0 python3 -u train.py --network r100 --loss arcface --dataset emore
```

### Using the server with the checkpoint

```shell script
cd /app
# simply run
python3 server.py
# or if you want to train with your own model
python3 server.py --model /path/to/model,0
```

### Using the client
```shell script
cd /app
# To register face information,
python3 client.py --input_image /path/to/face_image --mode SET_FACE
# To check whether it is recognized or not,
python3 client.py --input_image /path/to/face_image --mode RECOG_FACE
# To get the pure embedding vector from corresponding image,
python3 client.py --input_image /path/to/face_image --mode GET_VECTOR
```