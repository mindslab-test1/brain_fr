import sys
import os
import uuid
import time
import argparse
import grpc
import json
import cv2
import io
import numpy as np
import logging

from concurrent import futures

from deploy import face_model

from maum.brain.insight.insight_pb2 import FaceVector, FaceSet, FaceRecog
from maum.brain.insight.insight_pb2_grpc import InsightFaceServicer, add_InsightFaceServicer_to_server


def parse_args():
    parser = argparse.ArgumentParser(description='face model test')
    # general
    parser.add_argument('--image-size', default='112,112', help='')
    parser.add_argument('--model', default='/root/.insightface/models/model-r100-ii/model,0', help='path to load model.')
    parser.add_argument('--gpu', default=0, type=int, help='gpu id')
    parser.add_argument('-p', '--port', default=56101, type=int, help='server port')
    parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
    parser.add_argument('--db_root', default='./face_db.json', help='pivot data root path')
    parser.add_argument('--tmp_folder', default='./tmp', help='tmp image location')
    parser.add_argument('--log_level', default='INFO', type=str, help='logger level')

    return parser.parse_args()


class InsightFaceServer(InsightFaceServicer):
    def __init__(self, args):
        logging.info(args)
        vec = args.model.split(',')
        model_prefix = vec[0]
        model_epoch = int(vec[1])
        self.model = face_model.FaceModel(args.gpu, model_prefix, model_epoch)

        self.feat_db_name = args.db_root
        self.feat_db = {}
        if os.path.exists(self.feat_db_name):
            try:
                with open(self.feat_db_name) as f:
                    self.feat_db = json.load(f)
            except ValueError:
                self.feat_db = {}

        self.thres = 1.24  # fixed

    def GetFaceVector(self, request, context):
        try:
            img = cv2.imdecode(np.fromstring(request.faceImg, dtype=np.uint8), cv2.IMREAD_COLOR)
            img_ft = self.get_feature_from_image(img)

            ret_feature = FaceVector()
            ret_feature.faceVector.extend(list(img_ft))

            logging.debug("ret face vector:%s", img_ft)
            return ret_feature
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def SetFace(self, request, context):
        try:
            img = cv2.imdecode(np.fromstring(request.faceImg, dtype=np.uint8), cv2.IMREAD_COLOR)
            img_ft = self.get_feature_from_image(img)
            ret = FaceSet()
            if len(img_ft) == 0:
                ret.success = False
                return ret
            else:
                self.set_vector_to_db(request.name, img_ft)
                ret.success = True
                return ret
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def RecogFace(self, request, context):
        try:
            img = cv2.imdecode(np.fromstring(request.faceImg, dtype=np.uint8), cv2.IMREAD_COLOR)
            img_ft = self.get_feature_from_image(img)

            ret = FaceRecog()
            ret.name = "None"
            ret.success = False
            if len(img_ft) == 0:
                return ret
            name = self.compare_with_db(img_ft)
            logging.debug("compare with db")
            ret.name = name
            if name != "None":
                ret.success = True

            return ret
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def save_to_path(self, byte_file):
        logging.debug("in save to path")
        tmp_fname = str(uuid.uuid4())
        with open(os.path.join(self.tmp_root, tmp_fname), "wb") as f:
            f.write(byte_file)

        logging.debug("ret save to path")
        return tmp_fname

    def get_feature_from_image(self, in_img):
        img = self.model.get_input(in_img)
        if img is None:
            logging.debug('No face detected in %s', in_img)
            img_ft = []
        else:
            img_ft = self.model.get_feature(img)
        logging.debug("ret get feature")
        return img_ft

    def set_vector_to_db(self, name, img_ft):
        logging.debug("in set to db feature")
        self.feat_db[name] = img_ft.tolist()
        with open(self.feat_db_name, "w") as f:
            json.dump(self.feat_db, f)

    def compare_with_db(self, img_ft):
        min_dist = 10000
        name = "None"
        for name_key in self.feat_db.keys():
            if len(img_ft.tolist()) != len(self.feat_db[name_key]):
                dist = 10001
            else:
                dist = np.sum(np.square(img_ft - np.asarray(self.feat_db[name_key])))
            if dist < self.thres and dist < min_dist:
                min_dist = dist
                name = name_key

        return name


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    insight_server = InsightFaceServer(args)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),)
    insight_servicer = add_InsightFaceServicer_to_server(insight_server, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.info('insight face starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
