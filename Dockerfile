FROM nvcr.io/nvidia/cuda:10.0-cudnn7-runtime-ubuntu18.04

RUN DEBIAN_FRONTEND=noninteractive apt-get -qq update \
 && DEBIAN_FRONTEND=noninteractive apt-get -qqy install python3-pip ffmpeg git less nano unzip\
 && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install --upgrade pip setuptools
RUN python3 -m pip install mxnet-cu100 mxboard
RUN python3 -m pip install insightface
RUN python3 -m pip install grpcio grpcio-tools
ENV MXNET_CUDNN_AUTOTUNE_DEFAULT 0

COPY . /app
RUN python3 -m grpc.tools.protoc --proto_path=/app/ --python_out=/app/ --grpc_python_out=/app/ /app/maum/brain/insi

WORKDIR /app

EXPOSE 56101

ENTRYPOINT ["python3", "-u", "/app/server.py"]
